class Author < ApplicationRecord
	has_many :books, dependent: :destroy
	validates :author, presence: true, length: { maximum:255 },
                    uniqueness: { case_sensitive: false }
end
