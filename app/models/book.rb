class Book < ApplicationRecord
	belongs_to :category
	belongs_to :author
	belongs_to :publisher
	has_many :reviews, dependent: :destroy
	validates :name, presence: true
	validates :book_image, presence: true
    has_attached_file :book_image, styles: { book_index: "250x350>", book_show: "325x475>" }, default_url: "/images/:style/missing.png"
    validates_attachment_content_type :book_image, content_type: /\Aimage\/.*\z/ 
end
