class Review < ApplicationRecord
	belongs_to :book
	belongs_to :user
	validates :rate, presence: true
	validates :comment, presence: true, length: { maximum: 140 }
end
