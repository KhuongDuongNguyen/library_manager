class Category < ApplicationRecord
	has_many :books, dependent: :destroy
	validates :types, presence: true, length: { maximum:30 },
                    uniqueness: { case_sensitive: false }
end
