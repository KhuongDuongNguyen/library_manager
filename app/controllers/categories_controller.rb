class CategoriesController < ApplicationController

  def new
  	@category = Category.new
  end
  
  def show
  	@category = Category.paginate(page: params[:page])
  end

  def create
  	@category = Category.new(category_params)
  	if @category.save
  		flash[:success] = "Category added"
  		redirect_to '/newbook'
    else
    	render 'new'
    end
  end

  def destroy
  	Category.find(params[:id]).destroy
  	flash[:success] = "Category deleted"
  	redirect_to newbook_path
  end

  private

    def category_params
      params.require(:category).permit(:types)
    end
end
