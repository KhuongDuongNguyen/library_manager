class AuthorsController < ApplicationController

  def index
    @author = Author.where("author LIKE ?","%#{params[:search]}%")
  end

  def new
  	@author = Author.new
  end

  def create
  	@author = Author.new(author_params)
  	if @author.save
  		flash[:success] = "Author added"
  		redirect_to '/newbook'
    else
    	render 'new'
    end
  end

  def destroy
  	Author.find(params[:id]).destroy
  	flash[:success] = "Author deleted"
  	redirect_to newbook_path
  end

  private

    def author_params
      params.require(:author).permit(:author)
    end
end