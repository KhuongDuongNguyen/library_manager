class PublishersController < ApplicationController
  def new
  	@publisher = Publisher.new
  end
  
  def show
  	@publisher = Publisher.paginate(page: params[:page])
  end

  def create
  	@publisher = Publisher.new(publisher_params)
  	if @publisher.save
  		flash[:success] = "Publisher added"
  		redirect_to '/newbook'
    else
    	render 'new'
    end
  end

  def destroy
  	Publisher.find(params[:id]).destroy
  	flash[:success] = "Publisher deleted"
  	redirect_to newbook_path
  end

  private

    def publisher_params
      params.require(:publisher).permit(:name)
    end
end
