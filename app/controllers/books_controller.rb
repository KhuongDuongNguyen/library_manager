class BooksController < ApplicationController
  before_action :find_book, only: [:show, :edit, :update, :destroy]

  def new
  	@book = Book.new
    @categories = Category.all.map{ |c| [c.types,c.id] }
    @authors = Author.all.map{ |c| [c.author,c.id] }
    @publishers = Publisher.all.map{ |c| [c.name,c.id] }
  end

  def create
  	@book = Book.new(book_params)
    @book.category_id = params[:category_id]
    @book.author_id = params[:author_id]
    @book.publisher_id = params[:publisher_id]
  	if @book.save
  		redirect_to root_path
    else
    	render 'new'
    end
  end

  def index
    if params[:category].blank?
  	  @book = Book.all.order("created_at DESC")
    else
      @category_id = Category.find_by(types: params[:category]).id
      @book = Book.where(:category_id => @category_id).order("created_at DESC")
    end
    @book = Book.where("name LIKE ?","%#{params[:search]}%")
  end

  def show
    if @book.reviews.blank?
      @average_review = 0
    else
      @average_review = @book.reviews.average(:rate).round(2)
    end
  end

  def review
    @review = @book.reviews.build if logged_in?
  end
  
  def edit
    @categories = Category.all.map{ |c| [c.types,c.id] }
    @authors = Author.all.map{ |c| [c.author,c.id] }
    @publishers = Publisher.all.map{ |c| [c.name,c.id] }

  end

  def update
    @book.category_id = params[:category_id]
    @book.author_id = params[:author_id]
    @book.publisher_id = params[:publisher_id]
    if @book.update(book_params)
      flash[:success] = "Book updated"
      redirect_to @book
    else
      render 'edit'
    end
  end

  def destroy
  	@book.destroy
  	redirect_to root_path
  end

  private
    def book_params
    	params.require(:book).permit(:name,:book_image,:description,:year,:category_id,
                                                             :author_id,:publisher_id)
    end

    def find_book
    	@book = Book.find(params[:id])
    end
end
