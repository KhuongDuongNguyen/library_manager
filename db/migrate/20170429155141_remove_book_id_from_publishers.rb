class RemoveBookIdFromPublishers < ActiveRecord::Migration[5.0]
  def change
    remove_column :publishers, :book_id, :integer
  end
end
