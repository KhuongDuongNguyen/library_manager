class AddBookIdToPublishers < ActiveRecord::Migration[5.0]
  def change
    add_column :publishers, :book_id, :integer
  end
end
