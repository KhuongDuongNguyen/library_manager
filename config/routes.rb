Rails.application.routes.draw do


  root  'books#index'
  get   '/editbook'    ,to: 'books#edit'   
  get   '/newbook'     ,to: 'books#new'
  get   'books'        ,to: 'books#show'
  get   '/signup'      ,to: 'users#new'
  post  '/signup'      ,to: 'users#create'
  get   '/login'       ,to: 'sessions#new'
  post  '/login'       ,to: 'sessions#create'
  delete'/logout'      ,to: 'sessions#destroy'
  get   '/edit'        ,to: 'users#edit'
  get   '/index'       ,to: 'users#index'
  post  '/authors'     ,to: 'authors#create'
  get   '/authors'     ,to: 'authors#new'
  get   '/allauthors'  ,to: 'authors#index'
  get   '/newcategory' ,to: 'categories#new'
  get   '/newpublisher',to: 'publishers#new'


  get   'categories/show'
  
  resources :publishers
  resources :authors
  resources :categories
  resources :users
  resources :books do
    resources :reviews
  end
  
end
